import { Component, OnInit } from '@angular/core';
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { TimelineLite } from 'gsap';
import { TimelineMax } from 'gsap';

gsap.registerPlugin(ScrollTrigger);

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    const logoTL = gsap.timeline();

    logoTL
      .fromTo('.logo', 1, { opacity: 0 }, { opacity: 1 }, '+=0.5')
      .fromTo('.extremo-i', 0.5, { x: +25 }, { x: 0 }, '+=0.5')
      .fromTo('.extremo-d', 0.5, { x: -25 }, { x: 0 }, '-=0.5')
      .fromTo('.dentro1', 1, { opacity: 0 }, { opacity: 1 }, '-=0.3')
      .fromTo('.dentro2', 1, { opacity: 0 }, { opacity: 1 }, '-=0.9')
      .fromTo('.dentro3', 1, { opacity: 0 }, { opacity: 1 }, '-=0.9')
      .fromTo('.dentro4', 1, { opacity: 0 }, { opacity: 1 }, '-=0.9')
      .fromTo('.b1', 1, { opacity: 0 }, { opacity: 1 }, '-=0.9')
      .fromTo('.b2', 1, { opacity: 0 }, { opacity: 1 }, '-=0.9')
      .fromTo('.b3', 1, { opacity: 0 }, { opacity: 1 }, '-=0.9')
      .fromTo('.b4', 1, { opacity: 0 }, { opacity: 1 }, '-=0.9');

    const navbarTL = gsap.timeline({
      scrollTrigger: {
        trigger: 'app-navbar',
        markers: false,
        start: 'bottom 70px',
        end: 'bottom 0px',
        scrub: true,
      },
    });

    navbarTL.fromTo(
      'app-navbar',
      { background: 'rgb(250, 250, 250)' },
      { backgroundColor: 'white' }
    );
  }
}
