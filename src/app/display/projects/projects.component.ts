import { Component, OnInit } from '@angular/core';
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { TimelineLite } from 'gsap';
import { TimelineMax } from 'gsap';

gsap.registerPlugin(ScrollTrigger);

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
})
export class ProjectsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    const projectsTL = gsap.timeline({
      scrollTrigger: {
        trigger: '.p',
        markers: false,
        start: 'bottom 85%',
        end: 'bottom 30%',
        scrub: true,
      },
    });

    projectsTL
      .fromTo('.p', { color: 'transparent', x: -25 }, { color: 'black', x: 0 })
      .fromTo('.p1', { opacity: 0 }, { opacity: 1 })
      .fromTo('.p2', { opacity: 0 }, { opacity: 1 });
  }
}
