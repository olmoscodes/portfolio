import { Component, OnInit } from '@angular/core';
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { TimelineLite } from 'gsap';
import { TimelineMax } from 'gsap';

gsap.registerPlugin(ScrollTrigger);

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css'],
})
export class SkillsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    const skillsTL = gsap.timeline({
      scrollTrigger: {
        trigger: '.s',
        markers: false,
        start: 'bottom 85%',
        end: 'bottom 40%',
        scrub: true,
      },
    });

    skillsTL
      .fromTo('.s', { color: 'transparent', x: -25 }, { color: 'black', x: 0 })
      .fromTo('.skills', { opacity: 0 }, { opacity: 1 });
  }
}
