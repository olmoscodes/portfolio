import { Component, OnInit } from '@angular/core';
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { TimelineLite } from 'gsap';
import { TimelineMax } from 'gsap';

gsap.registerPlugin(ScrollTrigger);

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const contactTL = gsap.timeline({
      scrollTrigger: {
        trigger: '.c',
        markers: false,
        start: 'bottom 95%',
        end: 'bottom 83%',
        scrub: true,
      },
    });

    contactTL.fromTo('app-contact', { backgroundColor: 'rgb(250, 250, 250)' }, { backgroundColor: 'white' })
    .fromTo('.c', { color: 'transparent' }, { color: 'black' })
    .fromTo('.mail', { color: 'transparent', x: -25 }, { color: 'black' , x: 0})
    .fromTo('.telf', { color: 'transparent', x: -25 }, { color: 'black' , x: 0});

  }

}
