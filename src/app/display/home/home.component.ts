import { Component, OnInit } from '@angular/core';
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { TimelineLite } from 'gsap';
import { TimelineMax } from 'gsap';

gsap.registerPlugin(ScrollTrigger);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    const homeTL = gsap.timeline()

    homeTL.fromTo('.t', 1, {opacity: 0, x: -30}, {opacity: 1, x: 0}, '+=4.5')
    .fromTo('.subtitle', 1, {opacity: 0, x: -30}, {opacity: 1, x: 0}, '-=0.5')
    .fromTo('.t1', 1, {opacity: 0, x: -30}, {opacity: 1, x: 0}, '-=0.5')
    .fromTo('.t2', 1, {opacity: 0, x: -30}, {opacity: 1, x: 0}, '-=0.9')
    .fromTo('.t3', 1, {opacity: 0, x: -30}, {opacity: 1, x: 0}, '-=0.9')
    .fromTo('.photo', 1, {opacity: 0}, {opacity: 1}, '-=0.5')



  }

}
